﻿function submitForm() {

    alert("Please fill all date ranges to run this report");

    //var d1 = document.getElementById("sStartDate_mem").valueOf();
    //var d1 = document.getElementById("sEndtDate_mem").valueOf();
    //var d1 = document.getElementById("sStartDate_evt").valueOf();
    //var d1 = document.getElementById("sEndtDate_evt").valueOf();

    //if (checkForm(d1) && checkForm(d2) && checkForm(d3) && checkForm(d4)) {
    //    document.EntryForm.submit();
    //}
    //else {
    //    alert("Please fill all date ranges to run this report");
    //}

};

function checkForm(sDate1) {
    // regular expression to match required date format
    re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;

    if (sDate1 != ' ' && !sDate1.match(re)) {
        return false;
    }

    return true;
}


$(function () {
    $(".datepicker").datepicker({
        showOn: "button",
        buttonImage: "/CRM/Themes/img/color/Buttons/calCalendar.gif",
        buttonImageOnly: true,
        dateFormat: 'dd/mm/yy',
        maxDate: 0,
        changeYear: true,
        changeMonth: true,
        selectOtherMonths: true,
        maxDate: "+100m +100w",
        showOtherMonths: true,
        showButtonPanel: true
    });
});

$(document).ready(function () {
    //Make AJAX call to populate select boxes 
    var sFileLocation = 'CustomPages/Enbu/EOYReport/Includes/AjaxCalls.asp';
    // Method to call on the ASP page 
    var sMethodName = "getDropdownOptions";
    // Pass parameters and start with & 
    var sParams = "";

    //Result as string
    var sResult = callJQueryAjax(sFileLocation, sMethodName, sParams, sResult);
    if (sResult != "") {
        $('#selectedCompanies').html(sResult);
    }
});

//Make Ajax call to refresh Program List after keystrokes
$('#sFilter').keyup(function () {
    var sCompName = $(this).val();
    // Location of ASP page 
    var sFileLocation = 'CustomPages/Enbu/EOYReport/Includes/AjaxCalls.asp';
    // Method to call on the ASP page 
    var sMethodName = "getDropdownOptions";

    // Pass parameters and start with & 
    var sParams = "&compfilter=" + sCompName;


    var sResult = callJQueryAjax(sFileLocation, sMethodName, sParams, sResult);
    if (sResult != "") {
        $('#selectedCompanies').html(sResult);
    }
})

//Takes the Selection and passes values to a hidden form input
$("#selectedCompanies").click(function () {

    var selectedNames = [];
    var selectedIDs = [];
    $("#selectedCompanies :selected").each(function () {
        selectedNames.push($(this).text());
        selectedIDs.push($(this).val());
    });
    $('#selectedCompanies_names').val(selectedNames);
    $('#selectedCompanies_ids').val(selectedIDs);
    return false;
});

//Get Results
function callJQueryAjax(sFileLocation, sMethodName, sParams, sResult) {
    // This gets the query string from the SID onwards so includes all keys 
    var sStrQS = location.href.split(/\?/)[1];
    // This gets populated with http/https + server name + install name 
    var sStrAddr;
    if (window.location.toString().toLowerCase().search('eware.dll') == -1) {
        sStrAddr = window.location.toString().split('CustomPages')[0];
    } else {
        sStrAddr = window.location.toString().split('eware.dll')[0];
    }
    // Add all together along with the file location you provide and any parameters you wish to forward 
    var sStrURL = sStrAddr + sFileLocation + '?' + sStrQS + "&methodName=" + sMethodName + sParams;
    // Get the result as a response and return 
    return $.ajax({ url: sStrURL, async: false }).responseText;
}








