﻿using British_and_Irish_Chamber_of_Commerce.eWare;
using British_and_Irish_Chamber_of_Commerce.Includes;
using eWare;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace British_and_Irish_Chamber_of_Commerce
{
    public partial class DublinChamber : System.Web.UI.Page
    {
        String outputOption;
        String sMemStart;
        String sMemEnd;
        String sEvntStart;
        String sEvntEnd;
        String sCompanyIDs;
        String sFormatOption;
        String sSpacingAmount;
        String sGroupHeadings;

        Boolean validDates = true;

        protected void Page_Load(object sender, EventArgs e)
        {
            #region get POST data and check date inputs

            outputOption = Request.Form["_sOutput"];
            sMemStart = Request.Form["_sStartDate_mem"];
            sMemEnd = Request.Form["_sEndDate_mem"];
            sEvntStart = Request.Form["_sStartDate_evt"];
            sEvntEnd = Request.Form["_sEndDate_evt"];
            sCompanyIDs = Request.Form["_selectedCompanies_ids"];
            sFormatOption = Request.Form["_sFormat"];
            sSpacingAmount = Request.Form["sSpacing"];
            sGroupHeadings = Request.Form["_sgroupHeading"];
            if (sCompanyIDs == null) sCompanyIDs = "";
            if (sSpacingAmount == null) sSpacingAmount = "1";
            if (sGroupHeadings == null) sGroupHeadings = "2";
            int iSpacing = Int32.Parse(sSpacingAmount);
            Boolean bShowGroupHeadings = false;
            if (sGroupHeadings == "1") bShowGroupHeadings = true;

            if (CheckDate(sMemStart) && CheckDate(sMemEnd) && CheckDate(sEvntStart) && CheckDate(sEvntEnd))
            {
                DateTime dMemStart = Convert.ToDateTime(sMemStart);
                DateTime dMemEnd = Convert.ToDateTime(sMemEnd);
                DateTime dEvtStart = Convert.ToDateTime(sEvntStart);
                DateTime dEvtEnd = Convert.ToDateTime(sEvntEnd);

                sMemStart = dMemStart.ToLongDateString();
                sMemEnd = dMemEnd.ToLongDateString();
                sEvntStart = dEvtStart.ToLongDateString();
                sEvntEnd = dEvtEnd.ToLongDateString();

                if (sMemStart == "01 January 0001" || sMemStart == " ") sMemStart = "";
                if (sMemEnd == "01 January 0001" || sMemEnd == "") sMemEnd = "";
                if (sEvntStart == "01 January 0001" || sEvntStart == "") sEvntStart = "";
                if (sEvntEnd == "01 January 0001" || sEvntEnd == " ") sEvntEnd = "";
            }
            else
            {
                validDates = false;
            }

            #endregion

            #region Intialise Page and Container

            eWareConnector eWareConn = new eWareConnector();
            eWareBase eWare = eWareConn.Init();
            Includes.EnbuUIFunctions EnbuUI = new Includes.EnbuUIFunctions(eWare);


            EnbuUIFunctions eUI = new EnbuUIFunctions(eWare);

            //Attach javascript/CSS to Page
            LiteralControl javascriptRef = new LiteralControl("<script type='text/javascript' src='Includes/EnbuUtils.js'></script>");
            LiteralControl styleSheetRef = new LiteralControl("<link rel='stylesheet' href='Includes/OppertunityStyle.css'>");
            Page.Header.Controls.Add(javascriptRef);
            Page.Header.Controls.Add(styleSheetRef);

            //Setup Container and buttons
            var container = eWare.GetBlock("container");
            container.DisplayButton[1] = false;
            var sSubmitButton = eWare.Button("Run Report", "continue.gif", "javascript:submitForm();");
            container.AddButton(sSubmitButton);

            var parameterBlock = eWare.GetBlock("content");
            parameterBlock.Contents += eUI.enbu_UI_draw_panel("EOY Report", buildParameters(), 3, "", " style='width: 400px'");

            container.AddBlock(parameterBlock);

            #endregion

            //Data Validateion
            if (validDates)
            {
                #region Setup initial DataTables

                //Build List of Companies to Loop through
                DataTable companiesTable = new DataTable();
                DataTable personsTable = new DataTable();
                DataTable eventsTable = new DataTable();
                DataTable communicationsTable = new DataTable();

                //Get SQL Queries
                String sSQLc = getSQLString("Companies", sMemStart, sMemEnd, sCompanyIDs, sEvntStart, sEvntEnd);
                String sSQLp = getSQLString("Persons", sMemStart, sMemEnd, sCompanyIDs, sEvntStart, sEvntEnd);
                String sSQLe = getSQLString("Events", sMemStart, sMemEnd, sCompanyIDs, sEvntStart, sEvntEnd);
                String sSQLcoms = getSQLString("Communications", sMemStart, sMemEnd, sCompanyIDs, sEvntStart, sEvntEnd);

                //Response.Write(sSQLc + "<br>" + sSQLe + "<br>" + sSQLp + "<br>" + sSQLcoms);
                //Response.End();

                //Populate DataTables by executing SQL queries
                companiesTable = SpreadSheet.SelectSQL(sSQLc, eWare);
                personsTable = SpreadSheet.SelectSQL(sSQLp, eWare);
                eventsTable = SpreadSheet.SelectSQL(sSQLe, eWare);
                communicationsTable = SpreadSheet.SelectSQL(sSQLcoms, eWare);

                

                #endregion

                #region Setup FieldLists required for each datatable

                //Create FieldList for each
                SpreadSheet.FieldCollection fcCompanies = new SpreadSheet.FieldCollection();
                fcCompanies.InsertField("Comp_CompanyID", "CompanyID1", false, false, 30);
                fcCompanies.InsertField("Comp_Name", "Company", true, false, 70);

                SpreadSheet.FieldCollection fcPeople = new SpreadSheet.FieldCollection();
                fcPeople.InsertField("Comp_CompanyID", "CompanyID", false, false, 30);
                fcPeople.InsertField("Person", "Name", true, false, 70);

                SpreadSheet.FieldCollection fcEvents = new SpreadSheet.FieldCollection();
                fcEvents.InsertField("comp_companyid", "CompanyID", false, false, 30);
                fcEvents.InsertField("even_Name", "Event", true, false, 70);
                fcEvents.InsertField("People", "Attendees", true, false, 70);

                SpreadSheet.FieldCollection fcComms = new SpreadSheet.FieldCollection();
                fcComms.InsertField("Comp_CompanyID", "CompanyID", false, false, 30);
                fcComms.InsertField("comm_Type", "Type", false, false, 70); //Hidden Field so it can be referred to in code - but is not rendered in output
                fcComms.InsertField("Comm_DateTime", "Date", true, false, 70);
                fcComms.InsertField("comm_Note", "Description", true, false, 70);
                fcComms.InsertField("comm_eoyactivity", "Activity", true, false, 70);
                fcComms.InsertField("comm_eoysubject", "Subject", true, false, 70);

                #endregion

                //Create new report
                SpreadSheet.EnbuReport FullReport = new SpreadSheet.EnbuReport();

                //Check that there are results to render
                if (companiesTable.Rows.Count > 0)
                {
                    #region Loop through companies and add worksheets/data as require

                    //Create Worksheets
                    for (int i = 0; i < companiesTable.Rows.Count; i++)
                    {

                        //Create a Worksheet with the Company Name
                        String currentCompanyName = companiesTable.Rows[i]["Comp_Name"].ToString();
                        String currentCompanyID = companiesTable.Rows[i]["Comp_CompanyID"].ToString();
                        if (currentCompanyName == null) currentCompanyName = "Company1";
                        FullReport.CreateWorkSheet(currentCompanyName);


                        //Create a new DataBlock (Datatable and FieldList) and populate it from the main DataTable  - getReportData gets a subset of the main database
                        DataBlock personDataBlock = SpreadSheet.getReportData(personsTable, "Comp_CompanyID", currentCompanyID, fcPeople);
                        personDataBlock.titleString = "Key People";
                        personDataBlock.SubTitles.Add("Between 01/01/2000 and 01/01/2019");
                        personDataBlock.showColumnHeaders = true;
                        //Add DataObject to Worksheet with Title "Key People"
                        SpreadSheet.addReportData(FullReport, personDataBlock, currentCompanyName);


                        //Create a new DataBlock (Datatable and FieldList) and populate it from the main DataTable - getReportData gets a subset of the main database
                        DataBlock eventDataBlock = SpreadSheet.getReportData(eventsTable, "Comp_CompanyID", currentCompanyID, fcEvents);
                        eventDataBlock.titleString = "Attended Events";
                        eventDataBlock.showColumnHeaders = true;
                        //Add DataObject to Worksheet with Title "Events Attended"
                        SpreadSheet.addReportData(FullReport, eventDataBlock, currentCompanyName);


                        //Create a new DataBlock (Datatable and FieldList) and populate it from the main DataTable                        
                        DataBlock commsReportObject = SpreadSheet.getReportData(communicationsTable, "Comp_CompanyId", currentCompanyID, fcComms);
                        //The call GroupBY method to break that object up into sub groups
                        SpreadSheet.createGroupsBy(FullReport, commsReportObject, currentCompanyName, "Comm_Type", bShowGroupHeadings);

                    }

                    #endregion

                    #region Output either SpreadSheet or Render to Screen
                    if (outputOption == "1")
                    {
                        //Get HTML from this data
                        String sHTML = FullReport.ExportToHTML("<hr><br><br>");

                        ArrayList content = new ArrayList();
                        content.Add(sHTML);

                        var resultsBlock = eWare.GetBlock("content");
                        resultsBlock.Contents += eUI.enbu_UI_draw_panel("Results", content, 1, "", "");
                        container.AddBlock(resultsBlock);
                    }
                    else if (outputOption == "2")
                    {

                        if (sFormatOption == "1")
                        {
                            byte[] ep = FullReport.ExportToExcel("A1", true, iSpacing);
                            //////Render the response to the Browser
                            Response.Clear();
                            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                            Response.AddHeader("content-disposition", "attachment;  filename=OppertunityProgress.xlsx");
                            Response.BinaryWrite(ep);
                            Response.End();
                        }
                        else if (sFormatOption == "2")
                        {
                            byte[] ep = FullReport.ExportToExcel("A1", false, iSpacing);
                            //////Render the response to the Browser
                            Response.Clear();
                            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                            Response.AddHeader("content-disposition", "attachment;  filename=OppertunityProgress.xlsx");
                            Response.BinaryWrite(ep);
                            Response.End();
                        }
                        else
                        {
                            Response.Write("No Format Option selected");
                        }
                    }
                    else
                    {
                        Response.Write("No output Selected");
                    }

                    #endregion

                }
                else
                {
                    #region create NO Results Message

                    //Render No results Message
                    ArrayList al = new ArrayList();
                    al.Add("<h3>Please ensure all dates are filled in</h3>");

                    var resultsBlock = eWare.GetBlock("content");
                    resultsBlock.Contents += eUI.enbu_UI_draw_panel("Results", al, 1, "", "");
                    container.AddBlock(resultsBlock);
                    #endregion
                }

            }


            //Render the Page
            eWare.AddContent(container.Execute());
            Response.Write(eWare.GetPage());

        }

        protected bool CheckDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;
            }
            catch
            {
                return false;
            }
        }

        

        protected bool CheckDateDC(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private String getSQLString(String EntityName, String sMemStart, String sMemEnd, String CompanyIDs, String sEvtStart, String sEvtEnd)
        {
            String result = "";
            String membershipdateRange = "'" + sMemStart + " 00:00:00' and '" + sMemEnd + " 23:59:59'";
            String eventdateRange = "'" + sEvtStart + " 00:00:00' and '" + sEvtEnd + " 23:59:59'";

           String personSQL = "SELECT Company.Comp_CompanyId, Person.Pers_PersonId, Person.Pers_FirstName + ' ' + Person.Pers_LastName As Person, " +
                                "Person.Pers_Title, comp_validto " +
                                "FROM  Company INNER JOIN " +                                 
                                "Person ON Company.Comp_CompanyId = Person.Pers_CompanyId AND Company.Comp_Deleted is null " +
                                "WHERE comp_validto Between " + membershipdateRange;

            //This lists the people who have attended an event for a company as a single comma seperated string using the SQL STUFF command
            String eventSQl2 = "SELECT even_Name, Comp_CompanyId, " +
                                "people = STUFF(( SELECT ', ' + Replace(Pers_FirstName,' ','') + ' ' + Replace(Pers_LastName,' ','') As Person FROM vEventAttendees " +
                                "WHERE even_Name = x.even_Name AND Comp_CompanyId = x.Comp_CompanyId " +
                                "FOR XML PATH(''), TYPE).value('.[1]', 'nvarchar(max)'), 1, 2, '') " +
                                "FROM vEventAttendeesList AS x ";
                                if (CompanyIDs != "")
                                {
                                    eventSQl2 += "WHERE Comp_CompanyId in (" + CompanyIDs + ") ";
                                }
                                eventSQl2 += "GROUP BY even_Name, comp_companyid " +
                                "order by Comp_CompanyId ";
            
            String companiesSQL = "Select * from vCompMembershipReportFilter where comp_validto Between " + membershipdateRange;

            String commsSQL = "SELECT [Comm_Type],[Comm_Action],[Comm_Status],[Comm_Priority],[Comm_DateTime],[Comm_ToDateTime],[Comm_Note],[Comm_Deleted],[Comm_Description], [comm_eoyactivity], [comm_eoysubject], [Comp_CompanyId], " +
                                "[Comp_Name] ,[Comp_Type] ,[Comp_Status] ,[comp_validto] FROM [CRM_Live].[dbo].[vCommCompMembership] " +
                                "WHERE comp_validto Between " + membershipdateRange + " AND Comm_DateTime BETWEEN " + eventdateRange + " AND Comm_Deleted is null AND " +
                                "comm_includeineoy = 'Y'";




            if (EntityName == "Companies")
            {
                result = companiesSQL;
            }
            else if (EntityName == "Events")
            {
                result = eventSQl2;
            }
            else if (EntityName == "Persons")
            {
                result = personSQL;
            }
            else if (EntityName == "Communications")
            {
                result = commsSQL;
            }

            return result;
        }

        private ArrayList buildParameters()
        {


            String dateparams = "" +
                                "<table border=0>" +
                                "<tr><td></td><tdClass='VIEWBOXCAPTION'>Membership Date Range</td></tr>" +
                                "<tr><td>From</td><td><input type='Text' id='sStartDate_mem' name='_sStartDate_mem' class='datepicker' value='" + sMemStart + "'/></td></tr>" +
                                "<tr><td>To</td><td><input type='Text' id='sEndtDate_mem' name='_sEndDate_mem' class='datepicker' value='" + sMemEnd + "'/></td></tr>" +
                                "<tr><td><input type='hidden' id='selectedCompanies_names'/></td></tr>" +
                                "<tr><td><input type='hidden' id='selectedCompanies_ids' name='_selectedCompanies_ids'/></td></tr>" +
                                "<tr><td colspan=2><hr></td></tr>" +
                                "<tr><td colspan=2>Communication Date Range</td></tr>" +
                                "<tr><td>From</td><td><input type='Text' id='sStartDate_evt' name='_sStartDate_evt' class='datepicker' value='" + sEvntStart + "'/></td></tr>" +
                                "<tr><td>To</td><td><input type='Text' id='sEndtDate_evt' name='_sEndDate_evt' class='datepicker' value='" + sEvntEnd + "'/></td></tr>" +
                                "</table>";

            String compParams = "" +
                                "<table border=0>" +
                                "<tr><tdClass='VIEWBOXCAPTION'>Company Name <i>(Make Selection to Filter results)</i></td></tr>" +
                                "<tr><td><select multiple id='selectedCompanies' name='_selectedCompanies'  style='width: 250px' size=8></select></td></tr>" +
                                "<tr><td><input type='Text' id='sFilter' name='_sFilter' style='width: 250px'/><i>&nbspFilter</i></td></tr>" +
                                "</table>";

            String reportOptions = "" +
                                "<table border=0>" +
                                "<tr><td><br><b><i>Report Options</i></b></td><tr>" +
                                "<tr><td><select id='sOutput' name='_sOutput'  style='width: 300px'><option value='1'>Render to Screen</option><option value='2' selected>Export to Spreadsheet</option></select></td></tr>" +
                                "<tr><td><select id='sFormat' name='_sFormat'  style='width: 300px'><option selected value='1'>Formatted Output</option><option value='2'>No Formatting Applied</option></select></td></tr>" +
                                "<tr><td>Spacing Between Groups:</td></tr>" +
                                "<tr><td><select id='sSpacing' name='_sSpacing'  style='width: 50px'><option selected value='1'>1</option><option value='2'>2</option></option><option value='3'>3</option></select></td></tr>" +
                                "<tr><td>Headings on Communication Groups?</td></tr>" +
                                "<tr><td><select id='sgroupHeading' name='_sgroupHeading'  style='width: 50px'><option selected value='1'>Yes</option><option value='2'>No</option></select></td></tr>" +
                                "</table>";


            ArrayList contArry = new ArrayList();
            contArry.Add(compParams);
            contArry.Add(dateparams);
            contArry.Add(reportOptions);

            return contArry;
        }
    

    }
}