﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.IO;
using System.Web.UI.WebControls;
using OfficeOpenXml;
using System.Reflection;
using System.Data.SqlClient;
using System.Data;
using OfficeOpenXml.Style;
using System.Drawing;
using British_and_Irish_Chamber_of_Commerce.Includes;
using British_and_Irish_Chamber_of_Commerce.eWare;
using System.Collections;

namespace British_and_Irish_Chamber_of_Commerce
{
    public partial class Test : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            



            

 
        }

        private String getSQLString()
        {
            String result = "SELECT " +
                            "OpportunityProgress.Oppo_OpportunityId, " +
                            "OpportunityProgress.Oppo_OpportunityProgressId, " +
                            "Opportunity.Oppo_PrimaryCompanyId, " +
                            "Users.User_FirstName + ' ' + Users.User_LastName As userName, " +
                            "Company.Comp_Name, " +
                            "Opportunity.oppo_Description, " +
                            "OpportunityProgress.Oppo_Stage, " +
                            "OpportunityProgress.Oppo_Forecast, " +
                            "OpportunityProgress.Oppo_CreatedDate, " +
                            "Opportunity.oppo_projectstartdate, " +
                            "Opportunity.oppo_projectenddate " +
                            "FROM dbo.OpportunityProgress " +
                            "INNER JOIN Opportunity on OpportunityProgress.Oppo_OpportunityId = Opportunity.Oppo_OpportunityId " +
                            "INNER JOIN Company on Opportunity.Oppo_PrimaryCompanyId = Company.Comp_CompanyId " +
                            "INNER JOIN dbo.Users ON dbo.Opportunity.Oppo_AssignedUserId = dbo.Users.User_UserId " +
                            "WHERE OpportunityProgress.Oppo_OpportunityId in " +
                            "(Select Opportunity.Oppo_OpportunityId from Opportunity where Oppo_Opened Between '01/01/2000 09:00:00' AND '01/01/2018 09:00:00') " +
                            "AND OpportunityProgress.Oppo_Deleted is null " +
                            "ORDER BY userName, OpportunityProgress.Oppo_OpportunityId, OpportunityProgress.Oppo_CreatedDate  DESC";
            return result;
        }
    }
}